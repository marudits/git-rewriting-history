((username = "admin", password = "P@ssw0rd") => {
    if (!username || !password)
        return;
    
    var formLogin = document.getElementById("login-form");
    var usernameInput = formLogin.querySelector("input[name='username']");
    var passwordInput = formLogin.querySelector("input[name='password']");

    usernameInput.value = username;
    passwordInput.value = password;

    var btnLogin = formLogin.querySelector("#signin-btn");
    btnLogin.click();
})()